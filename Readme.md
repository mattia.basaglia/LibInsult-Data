LibInsult Data
==============

This repository contains word lists for libinsult.


License
-------

GPLv3+, see COPYING


Sources
-------

See http://insult.mattbas.org


Author
------

Mattia Basaglia <mattia.basaglia@gmail.com>
